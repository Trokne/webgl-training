import React from "react";
import "./app.css";
import Cube from "../cube/cube";
import Landscape from "../landscape/landscape";
import MultipleTriangles from "../multipletriangles/multipletriangles";
import PaintDrawer from "../paintdrawer/paintdrawer";
import MultiplePoints from "../multiplepoints/multiplepoints";
import ColoredOutput from "../coloredoutput/coloredoutput";
import TexturedQuad from "../texturedquad/texturedquad";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      components: [
        {
          name: "Рисование точек",
          value: <PaintDrawer />,
        },
        {
          name: "Анимированный реугольник",
          value: <MultiplePoints />,
        },
        {
          name: "Разные цвета через varying",
          value: <ColoredOutput />,
        },
        {
          name: "Текстурированный квадрат",
          value: <TexturedQuad />,
        },
        {
          name: "Направление взгляда: треугольники",
          value: <MultipleTriangles />,
        },
        {
          name: "Куб",
          value: <Cube />,
        },
        {
          name: "Ландшафт",
          value: <Landscape />,
        },
      ],
      selectedComponentIndex: 6,
    };
  }

  render() {
    return (
      <div className="App">
        <select
          onChange={this.changeSelectedComponent.bind(this)}
          value={this.state.selectedComponentIndex}
        >
          {this.state.components.map((component, index) => (
            <option value={index} key={index}>
              {component.name}
            </option>
          ))}
        </select>
        {this.state.components[this.state.selectedComponentIndex].value}
      </div>
    );
  }

  changeSelectedComponent(event) {
    this.setState({
      selectedComponentIndex: event.target.value,
    });
  }
}

export default App;
