import React from "react";
import { RefObject } from "react";
import * as webgl from "../webgl/webgl";
// @ts-ignore
import fragShader from "./fragment.glsl";
// @ts-ignore
import vertexShader from "./vertex.glsl";

interface IProps {}

interface IState {
  drawAreaReference: RefObject<HTMLCanvasElement>;
  gl: WebGLRenderingContext | null;
  verticesCount: number;
}

class ColoredOutput extends React.Component<IProps, IState> {
  public constructor(props: IProps) {
    super(props);
    this.state = {
      verticesCount: 0,
      drawAreaReference: React.createRef<HTMLCanvasElement>(),
      gl: null,
    };
  }

  public render() {
    return (
      <div>
        <canvas
          id="draw-area"
          width="500px"
          height="500px"
          ref={this.state.drawAreaReference}
        >
          Пожалуйста, воспользуйтесь современным браузером с поддержкой canvas!
        </canvas>
      </div>
    );
  }

  public componentDidMount() {
    const gl = webgl.initializeDrawArea(
      this.state.drawAreaReference.current,
      vertexShader,
      fragShader
    );

    webgl.clearColorBuffer(gl, 0, 0, 0, 1);

    this.setState(
      {
        gl: gl,
        verticesCount: this.initVertexBuffers(gl),
      },
      this.draw.bind(this)
    );
  }

  private draw() {
    const { gl, verticesCount } = this.state;

    gl?.drawArrays(gl!.TRIANGLES, 0, verticesCount);
  }

  private initVertexBuffers(gl: WebGLRenderingContext): number {
    const vertices = [
      [0.0, 0.5, 1.0, 0.0, 0.0],
      [-0.5, -0.5, 0.0, 1.0, 0.0],
      [0.5, -0.5, 0.0, 0.0, 1.0],
    ];
    const flatVertices = new Float32Array(vertices.flat());

    this.initializePositionBuffer(gl, flatVertices);
    this.initializeColorBuffer(gl, flatVertices);

    return vertices.length;
  }

  private initializePositionBuffer(
    gl: WebGLRenderingContext,
    vertices: Float32Array
  ) {
    const aPointPosition = webgl.getAttribute(gl, "a_Position");
    const elementSize = vertices.BYTES_PER_ELEMENT;
    webgl.createAndFillBuffer(gl, gl.ARRAY_BUFFER, vertices);
    webgl.linkAttributeToBuffer(
      gl,
      aPointPosition!,
      2,
      gl.FLOAT,
      false,
      elementSize * 5,
      0
    );
  }

  private initializeColorBuffer(
    gl: WebGLRenderingContext,
    vertices: Float32Array
  ) {
    const aColor = webgl.getAttribute(gl, "a_Color");
    const elementSize = vertices.BYTES_PER_ELEMENT;
    webgl.createAndFillBuffer(gl, gl.ARRAY_BUFFER, vertices);
    webgl.linkAttributeToBuffer(
      gl,
      aColor!,
      3,
      gl.FLOAT,
      false,
      elementSize * 5,
      elementSize * 2
    );
  }
}

export default ColoredOutput;
