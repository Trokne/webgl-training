import React, { RefObject } from "react";
import * as webgl from "../webgl/webgl";
// @ts-ignore
import fragShader from "./fragment.glsl";
// @ts-ignore
import vertexShader from "./vertex.glsl";
import { Mat4, Vec3 } from "cuon-matrix-ts";
import { colors, indices, normals, vertices } from "./vertices";

interface IProps {}

interface IState {
  canvasHeight: number;
  canvasWidth: number;
  drawAreaReference: RefObject<HTMLCanvasElement>;
  gl: WebGLRenderingContext | null;
  vertexBuffer: WebGLBuffer | null;
  indicesBuffer: WebGLBuffer | null;
  verticesCount: number;
}

class Cube extends React.Component<IProps, IState> {
  public constructor(props: IProps) {
    super(props);
    this.state = {
      canvasHeight: 500,
      canvasWidth: 500,
      verticesCount: 0,
      vertexBuffer: null,
      indicesBuffer: null,
      drawAreaReference: React.createRef<HTMLCanvasElement>(),
      gl: null,
    };
  }

  public render() {
    return (
      <div>
        <canvas
          id="draw-area"
          width={this.state.canvasWidth + "px"}
          height={this.state.canvasHeight + "px"}
          ref={this.state.drawAreaReference}
        >
          Пожалуйста, воспользуйтесь современным браузером с поддержкой canvas!
        </canvas>
      </div>
    );
  }

  public componentWillUnmount(): void {
    this.state.gl?.deleteBuffer(this.state.vertexBuffer);
  }

  public async componentDidMount() {
    const gl = webgl.initializeDrawArea(
      this.state.drawAreaReference.current,
      vertexShader,
      fragShader
    );
    const verticesCount = this.initializeBuffers(gl);
    this.setRotations(gl);
    this.setLight(gl);
    this.setState(
      {
        gl: gl,
        verticesCount: verticesCount,
      },
      this.draw.bind(this)
    );
  }

  private setRotations(gl: WebGLRenderingContext) {
    const { canvasWidth, canvasHeight } = this.state;
    const uViewMatrix = webgl.getUniform(gl, "u_ViewMatrix");
    const uModelMatrix = webgl.getUniform(gl, "u_NormalMatrix");
    const viewMatrix = new Mat4();
    const modelMatrix = new Mat4();

    modelMatrix.rotate(90, 0, 0, 1);

    viewMatrix
      .setPerspective(30, canvasWidth / canvasHeight, 1, 100)
      .lookAt(3, 3, 7, 0, 0, 0, 0, 1, 0)
      .multiply(modelMatrix);

    // Вектор нормали = исходный * обратная транспонированная матрица модели
    modelMatrix.setInverseOf(modelMatrix);
    modelMatrix.transpose();

    gl.uniformMatrix4fv(uViewMatrix, false, viewMatrix.elements);
    gl.uniformMatrix4fv(uModelMatrix, false, modelMatrix.elements);
  }

  private draw() {
    const { gl, verticesCount } = this.state;
    webgl.enablePolygonOffset(gl!, 1.0, 1.0);
    webgl.removeHideSurfaces(gl!);
    webgl.clearColorBuffer(gl!, 0, 0, 0, 1);
    gl!.drawElements(gl!.TRIANGLES, verticesCount, gl!.UNSIGNED_BYTE, 0);
  }

  private initializeBuffers(gl: WebGLRenderingContext) {
    webgl.initializeArrayBuffer(gl, "a_Position", vertices, 3, gl.FLOAT);
    webgl.initializeArrayBuffer(gl, "a_Color", colors, 3, gl.FLOAT);
    webgl.initializeArrayBuffer(gl, "a_Normal", normals, 3, gl.FLOAT);
    webgl.createAndFillBuffer(gl, gl.ELEMENT_ARRAY_BUFFER, indices);

    return indices.length;
  }

  private setLight(gl: WebGLRenderingContext) {
    const lightColor = new Float32Array([1.0, 1.0, 1.0]);
    const lightDirection = new Vec3(0.5, 3.0, 4.0);
    const ambientLight = new Float32Array([0.2, 0.2, 0.2]);
    const uLightColor = webgl.getUniform(gl, "u_LightColor");
    const uLightDirection = webgl.getUniform(gl, "u_LightDirection");
    const uAmbientLight = webgl.getUniform(gl, "u_AmbientLight");

    lightDirection.normalize();
    gl.uniform3fv(uLightDirection, lightDirection.elements);
    gl.uniform3fv(uLightColor, lightColor);
    gl.uniform3fv(uAmbientLight, ambientLight);
  }
}

export default Cube;
