attribute vec4 a_Position;
attribute vec4 a_Color;
attribute vec4 a_Normal;

uniform mat4 u_ViewMatrix;
/** Цвет света **/
uniform vec3 u_LightColor;
/** Нормализованные мировые координаты **/
uniform vec3 u_LightDirection;
/** Фоновое освещениее **/
uniform vec3 u_AmbientLight;
/** Матрица преобразования нормали **/
uniform mat4 u_NormalMatrix;

varying vec4 v_Color;

void main() {
    gl_Position = u_ViewMatrix * a_Position;
    // Нормализовать длину вектора нормали
    vec3 normal = normalize(vec3(u_NormalMatrix * a_Normal));
    // Скалярное произведение направления света на ориентацию поверхности
    float nDotL = max(dot(u_LightDirection, normal), 0.0);
    // Вычисление цвета в модели диф. отражения
    vec3 diffuse = u_LightColor * a_Color.rgb * nDotL;
    vec3 ambient = u_AmbientLight * a_Color.rgb;
    v_Color = vec4(diffuse + ambient, a_Color.a);
}