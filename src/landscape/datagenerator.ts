import * as heightmap from "../webgl/heightmap";

export function generateTextureCoordinates(
  heightMap: number[][]
): Float32Array {
  const coordinates = [];
  for (let i = 0; i < heightMap.length; i++) {
    const line = [];
    for (let j = 0; j < heightMap[i].length; j++) {
      if (i % 2 === 0 && j % 2 === 0) {
        line.push(0.0, 0.0);
      } else if (i % 2 === 0 && j % 2 !== 0) {
        line.push(0.0, 1.0);
      } else if (i % 2 !== 0 && j % 2 === 0) {
        line.push(0.0, 1.0);
      } else if (i % 2 !== 0 && j % 2 !== 0) {
        line.push(1.0, 1.0);
      }
    }
    coordinates.push(line);
  }

  return new Float32Array(coordinates.flat());
}

export function generateVertices(
  heightMap: number[][],
  gridSize: number
): Float32Array {
  const vertices = [];
  let x = 0.0;
  let z = 0.0;
  for (let i = 0; i < heightMap.length; i++) {
    const line = [];
    for (let j = 0; j < heightMap[i].length; j++) {
      const y = heightmap.calculateHeight(x, z, heightMap);
      line.push(x, y, z);
      if (j % 2 === 0) {
        x += gridSize;
      }
    }
    if (i % 2 === 0) {
      z += gridSize;
    }

    x = 0.0;
    vertices.push(line);
  }

  return new Float32Array(vertices.flat());
}

export function generateIndices(heightMap: number[][]): Uint16Array {
  /**
   * Обход b->a->c и b->c->d
   * a --- b
   * |     |
   * c --- d
   */
  const indices = [];
  let a = heightMap[0].length;
  let b = a + 1;
  let c = 0;
  let d = 1;

  for (let i = 0; i < heightMap.length / 2; i++) {
    const indicesLine = [];
    for (let j = 0; j < heightMap[i].length / 2; j++) {
      indicesLine.push(b, a, c, b, c, d);
      [a, b, c, d] = add([a, b, c, d], 2);
    }
    indices.push(indicesLine);
    [a, b, c, d] = add([a, b, c, d], heightMap[0].length);
  }

  return new Uint16Array(indices.flat());
}

function add(num: number[], count: number) {
  for (let i = 0; i < num.length; i++) {
    num[i] += count;
  }
  return num;
}
