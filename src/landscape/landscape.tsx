import React, { ChangeEvent, RefObject } from "react";
import * as webgl from "../webgl/webgl";
import * as heightmap from "../webgl/heightmap";
import * as webglVariables from "../webgl/webgl-variables";
// @ts-ignore
import landscapeFragmentShader from "./shaders/landscape/fragment.glsl";
// @ts-ignore
import landscapeVertexShader from "./shaders/landscape/vertex.glsl";
import { Mat4 } from "cuon-matrix-ts";
import HeightMapImage from "../assets/heightmap.png";
import "./landscape.css";
import * as dataGenerator from "./datagenerator";
import * as textureLoader from "../webgl/texture/textureloader";
import groundImage from "../assets/ground_1.jpg";
import { TextureParameter } from "../webgl/texture/texture-parameter";
import { getWebGLContext, initShaders } from "../webgl/cuon-utils";
import { Camera } from "../webgl/camera/camera";
import { Point } from "../utils/point";

interface IProps {}

interface IState {
  canvasHeight: number;
  canvasWidth: number;
  drawAreaReference: RefObject<HTMLCanvasElement>;
  gl: WebGLRenderingContext | null;
  verticesCount: number;
  landscapeBuffers: WebGLBuffer[];
  gridSize: number;
  camera: Camera;
  heightMap: number[][];
  landscapeProgram: WebGLProgram | null;
  cubeProgram: WebGLProgram | null;
}

class Landscape extends React.Component<IProps, IState> {
  public constructor(props: IProps) {
    super(props);
    this.state = {
      landscapeProgram: null,
      cubeProgram: null,
      canvasHeight: 800,
      canvasWidth: 800,
      drawAreaReference: React.createRef<HTMLCanvasElement>(),
      gl: null,
      verticesCount: 0,
      landscapeBuffers: [],
      gridSize: 1,
      camera: new Camera(),
      heightMap: [],
    };
  }

  public render() {
    return (
      <div className={"landscape-container"}>
        <canvas
          id="draw-area"
          width={this.state.canvasWidth + "px"}
          height={this.state.canvasHeight + "px"}
          ref={this.state.drawAreaReference}
        >
          Пожалуйста, воспользуйтесь современным браузером с поддержкой canvas!
        </canvas>
        <div className={"landscape-inputs"}>
          FOV:{" "}
          <input
            type={"number"}
            value={this.state.camera.fieldOfView}
            onChange={this.fovChange.bind(this)}
          />
        </div>
      </div>
    );
  }

  public fovChange(event: ChangeEvent<HTMLInputElement>) {
    const { camera } = this.state;
    camera.fieldOfView = Number(event.target.value);
    this.setState({ camera: camera }, this.draw.bind(this));
  }

  public async componentDidMount() {
    const canvas = this.state.drawAreaReference.current;
    const gl = getWebGLContext(canvas, true);
    const landscapeProgram = initShaders(
      gl,
      landscapeVertexShader,
      landscapeFragmentShader
    );

    this.setState(
      {
        gl: gl,
        landscapeProgram: landscapeProgram,
      },
      this.draw.bind(this)
    );

    this.subscribeEvents();
  }

  public componentWillUnmount(): void {}

  private async initializeObjects(gl: WebGLRenderingContext): Promise<number> {
    const { gridSize } = this.state;
    const landscapeBuffers = [];
    const heightMap = await heightmap.read(HeightMapImage);
    const vertices = dataGenerator.generateVertices(heightMap, gridSize);
    landscapeBuffers.push(
      webgl.initializeArrayBuffer(gl, "a_Position", vertices, 3, gl.FLOAT)
    );

    const indices = dataGenerator.generateIndices(heightMap);
    landscapeBuffers.push(
      webgl.createAndFillBuffer(gl, gl.ELEMENT_ARRAY_BUFFER, indices)
    );

    const textureCoords = dataGenerator.generateTextureCoordinates(heightMap);
    landscapeBuffers.push(
      webgl.initializeArrayBuffer(
        gl,
        "a_LandscapeTextureCoord",
        textureCoords,
        2,
        gl.FLOAT
      )
    );

    this.setState({
      landscapeBuffers: this.state.landscapeBuffers.concat(landscapeBuffers),
      heightMap: heightMap,
    });

    return indices.length;
  }

  private changeCameraPositionByKeyboard(event: KeyboardEvent) {
    const { camera } = this.state;
    camera.setNewUserPositionByKey(event.key, this.state.heightMap);

    this.setState(
      {
        camera: camera,
      },
      this.draw.bind(this)
    );
  }

  private changeCameraPositionByMouse(event: MouseEvent) {
    const { camera } = this.state;
    if (!camera.mouseCameraParameters.isLeftButtonPressed) {
      return;
    }

    camera.setNewUserPositionByMouse(new Point(event.clientX, event.clientY));

    this.setState(
      {
        camera: camera,
      },
      this.draw.bind(this)
    );
  }

  private initializeCamera(gl: WebGLRenderingContext) {
    const { canvasWidth, canvasHeight, camera } = this.state;

    const viewMatrix = new Mat4();

    viewMatrix
      .setPerspective(camera.fieldOfView, canvasWidth / canvasHeight, 1, 100)
      .lookAt(
        camera.current.x,
        camera.current.y,
        camera.current.z,
        camera.lookAt.x,
        camera.lookAt.y,
        camera.lookAt.z,
        camera.up.x,
        camera.up.y,
        camera.up.z
      );

    webglVariables.setMatrix4ToUniform(gl, "u_ViewMatrix", viewMatrix);
  }

  private draw() {
    this.drawLandscape();
  }

  private async initializeLandscapeTextures(gl: WebGLRenderingContext) {
    const uLandscapeTexture = webgl.getUniform(gl, "u_LandscapeTexture");
    const textureParameters: TextureParameter[] = [
      new TextureParameter(gl.TEXTURE_MAG_FILTER, gl.LINEAR),
      new TextureParameter(gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR),
      new TextureParameter(gl.TEXTURE_WRAP_S, gl.REPEAT),
      new TextureParameter(gl.TEXTURE_WRAP_T, gl.REPEAT),
    ];
    await textureLoader.initializeTexture(
      gl,
      uLandscapeTexture,
      groundImage,
      1,
      textureParameters
    );
    gl.generateMipmap(gl.TEXTURE_2D);
  }

  private async drawLandscape() {
    const { gl, landscapeProgram } = this.state;
    if (!gl) {
      return;
    }
    console.log("o_o");
    gl!.useProgram(landscapeProgram);

    const verticesCount = await this.initializeObjects(gl);
    await this.initializeLandscapeTextures(gl);
    this.initializeCamera(gl);
    webgl.enablePolygonOffset(gl!, 1.0, 1.0);
    webgl.removeHideSurfaces(gl!);
    webgl.clearColorBuffer(gl!, 0, 0, 0, 1);
    gl!.drawElements(gl!.TRIANGLES, verticesCount, gl!.UNSIGNED_SHORT, 0);
  }

  private subscribeEvents() {
    const canvas = this.state.drawAreaReference.current;
    const camera = this.state.camera;

    document.onkeydown = this.changeCameraPositionByKeyboard.bind(this);
    canvas!.onmousedown = () => camera.setMousePressed(true);
    canvas!.onmouseup = () => camera.setMousePressed(false);
    canvas!.onmousemove = this.changeCameraPositionByMouse.bind(this);
  }
}

export default Landscape;
