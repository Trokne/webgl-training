precision mediump float;

varying vec2 v_LandscapeTextureCoord;
uniform sampler2D u_LandscapeTexture;

void main() {
    gl_FragColor = texture2D(u_LandscapeTexture, v_LandscapeTextureCoord);
}
