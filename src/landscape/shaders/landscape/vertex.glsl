attribute vec4 a_Position;
attribute vec2 a_LandscapeTextureCoord;

uniform mat4 u_ViewMatrix;
varying vec2 v_LandscapeTextureCoord;

void main() {
    gl_Position = u_ViewMatrix * a_Position;
    v_LandscapeTextureCoord = a_LandscapeTextureCoord;
}