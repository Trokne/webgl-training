import React from "react";
import { RefObject } from "react";
import * as webgl from "../webgl/webgl";
// @ts-ignore
import fragShader from "./fragment.glsl";
// @ts-ignore
import vertexShader from "./vertex.glsl";
import { Mat4 } from "cuon-matrix-ts";

interface IProps {}

interface IState {
  drawAreaReference: RefObject<HTMLCanvasElement>;
  aPointPosition: GLint | null;
  currentAngle: number;
  modelMatrix: Mat4;
  uModelMatrix: WebGLUniformLocation | null;
  gl: WebGLRenderingContext | null;
  animationRequestDate: number;
  anglePerSecond: number;
  verticesCount: number;
}

class MultiplePoints extends React.Component<IProps, IState> {
  public constructor(props: IProps) {
    super(props);
    this.state = {
      verticesCount: 0,
      aPointPosition: null,
      uModelMatrix: null,
      modelMatrix: new Mat4(),
      currentAngle: 0,
      drawAreaReference: React.createRef<HTMLCanvasElement>(),
      gl: null,
      animationRequestDate: Date.now(),
      anglePerSecond: 45,
    };
  }

  public render() {
    return (
      <div>
        <canvas
          id="draw-area"
          width="500px"
          height="500px"
          ref={this.state.drawAreaReference}
        >
          Пожалуйста, воспользуйтесь современным браузером с поддержкой canvas!
        </canvas>
      </div>
    );
  }

  public componentDidMount() {
    const gl = webgl.initializeDrawArea(
      this.state.drawAreaReference.current,
      vertexShader,
      fragShader
    );

    const aPointPosition = webgl.getAttribute(gl, "a_Position");
    const uModelMatrix = webgl.getUniform(gl, "u_ModelMatrix");

    webgl.clearColorBuffer(gl, 0, 0, 0, 1);

    this.setState(
      {
        gl: gl,
        aPointPosition: aPointPosition,
        uModelMatrix: uModelMatrix,
        verticesCount: this.initVertexBuffer(gl, aPointPosition),
      },
      this.animate.bind(this)
    );
  }

  private initVertexBuffer(
    gl: WebGLRenderingContext,
    aPointPosition: GLint
  ): number {
    const coordsDimension = 2;
    const vertices = new Float32Array(
      [
        [0.0, 0.5],
        [-0.5, -0.5],
        [0.5, -0.5],
      ].flat()
    );
    webgl.createBuffer(gl, gl.ARRAY_BUFFER);
    gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

    gl.vertexAttribPointer(
      aPointPosition,
      coordsDimension,
      gl.FLOAT,
      false,
      0,
      0
    );
    gl.enableVertexAttribArray(aPointPosition);
    return vertices.length / coordsDimension;
  }

  private animate(): void {
    const now = Date.now();
    const elapsed = now - this.state.animationRequestDate;
    const newAngle =
      (this.state.currentAngle +
        (this.state.anglePerSecond * elapsed) / 1000.0) %
      360;

    this.setState(
      {
        animationRequestDate: now,
        currentAngle: newAngle,
      },
      this.draw.bind(this)
    );
  }

  private draw(): void {
    const {
      gl,
      uModelMatrix,
      modelMatrix,
      verticesCount,
      currentAngle,
    } = this.state;

    modelMatrix.setRotate(currentAngle, 0, 0, 1);

    gl!.uniformMatrix4fv(uModelMatrix, false, modelMatrix.elements);
    gl!.clear(gl!.COLOR_BUFFER_BIT);
    gl!.drawArrays(gl!.TRIANGLES, 0, verticesCount);
    requestAnimationFrame(this.animate.bind(this));
  }
}

export default MultiplePoints;
