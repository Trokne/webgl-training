import React, { RefObject } from "react";
import * as webgl from "../webgl/webgl";
// @ts-ignore
import fragShader from "./fragment.glsl";
// @ts-ignore
import vertexShader from "./vertex.glsl";
import { Mat4 } from "cuon-matrix-ts";

interface IProps {}

interface IState {
  canvasHeight: number;
  canvasWidth: number;
  drawAreaReference: RefObject<HTMLCanvasElement>;
  gl: WebGLRenderingContext | null;
  buffer: WebGLBuffer | null;
  verticesCount: number;
  currentEyePosition: number[];
  uViewMatrix: WebGLUniformLocation | null;
  viewMatrix: Mat4;
}

class MultipleTriangles extends React.Component<IProps, IState> {
  public constructor(props: IProps) {
    super(props);
    this.state = {
      canvasHeight: 500,
      canvasWidth: 500,
      verticesCount: 0,
      buffer: null,
      drawAreaReference: React.createRef<HTMLCanvasElement>(),
      gl: null,
      currentEyePosition: [0, 0, 5],
      uViewMatrix: null,
      viewMatrix: new Mat4(),
    };
  }

  public render() {
    return (
      <div>
        <canvas
          id="draw-area"
          width={this.state.canvasWidth + "px"}
          height={this.state.canvasHeight + "px"}
          ref={this.state.drawAreaReference}
        >
          Пожалуйста, воспользуйтесь современным браузером с поддержкой canvas!
        </canvas>
      </div>
    );
  }

  public componentWillUnmount(): void {
    this.state.gl?.deleteBuffer(this.state.buffer);
  }

  public async componentDidMount() {
    const gl = webgl.initializeDrawArea(
      this.state.drawAreaReference.current,
      vertexShader,
      fragShader
    );

    const verticesCount = this.initializeBuffer(gl);
    this.setUserView(gl);
    this.setState(
      {
        gl: gl,
        verticesCount: verticesCount,
      },
      this.draw.bind(this)
    );
  }

  private initializeBuffer(gl: WebGLRenderingContext): number {
    const coordsAndColors = [
      // Three triangles on the right side
      [0.75, 1.0, 0, 0.4, 1.0, 0.4], // The back green one
      [0.25, -1.0, 0, 0.4, 1.0, 0.4],
      [1.25, -1.0, 0, 1.0, 0.4, 0.4],

      [0.75, 1.0, -2.0, 1.0, 1.0, 0.4], // The middle yellow one
      [0.25, -1.0, -2.0, 1.0, 1.0, 0.4],
      [1.25, -1.0, -2.0, 1.0, 0.4, 0.4],

      [0.75, 1.0, -4, 0.4, 0.4, 1.0], // The front blue one
      [0.25, -1.0, -4, 0.4, 0.4, 1.0],
      [1.25, -1.0, -4, 1.0, 0.4, 0.4],

      // Three triangles on the left side
      [-0.75, 1.0, -0, 0.4, 1.0, 0.4], // The back green one
      [-1.25, -1.0, 0, 0.4, 1.0, 0.4],
      [-0.25, -1.0, 0, 1.0, 0.4, 0.4],

      [-0.75, 1.0, -2.0, 1.0, 1.0, 0.4], // The middle yellow one
      [-1.25, -1.0, -2.0, 1.0, 1.0, 0.4],
      [-0.25, -1.0, -2.0, 1.0, 0.4, 0.4],

      [-0.75, 1.0, -4, 0.4, 0.4, 1.0], // The front blue one
      [-1.25, -1.0, -4, 0.4, 0.4, 1.0],
      [-0.25, -1.0, -4, 1.0, 0.4, 0.4],
    ];

    const buffer = webgl.createAndFillBuffer(
      gl,
      gl.ARRAY_BUFFER,
      new Float32Array(coordsAndColors.flat())
    );
    this.setState({
      buffer: buffer,
    });

    this.assignBufferToVariables(gl, coordsAndColors);
    webgl.unbindBuffer(gl);
    return coordsAndColors.length;
  }

  private assignBufferToVariables(
    gl: WebGLRenderingContext,
    coordsAndColors: number[][]
  ) {
    const flatCoordsAndColors = new Float32Array(coordsAndColors.flat());
    const elementSize = flatCoordsAndColors.BYTES_PER_ELEMENT;
    const aPosition = webgl.getAttribute(gl, "a_Position");
    const aColor = webgl.getAttribute(gl, "a_Color");

    webgl.linkAttributeToBuffer(
      gl,
      aPosition,
      3,
      gl.FLOAT,
      false,
      elementSize * 6
    );

    webgl.linkAttributeToBuffer(
      gl,
      aColor,
      3,
      gl.FLOAT,
      false,
      elementSize * 6,
      elementSize * 3
    );
  }

  private setUserView(gl: WebGLRenderingContext) {
    const viewMatrix = webgl.getUniform(gl, "u_ViewMatrix");
    this.setState(
      {
        uViewMatrix: viewMatrix,
      },
      () => {
        document.onkeydown = this.rotateUserView.bind(this);
      }
    );
  }

  private rotateUserView(event: KeyboardEvent) {
    const { currentEyePosition } = this.state;
    const newEyePosition = this.getNewEyePosition(
      currentEyePosition,
      event.code
    );
    this.setState(
      {
        currentEyePosition: newEyePosition,
      },
      this.draw.bind(this)
    );
  }

  private draw() {
    const {
      gl,
      verticesCount,
      currentEyePosition,
      uViewMatrix,
      viewMatrix,
      canvasWidth,
      canvasHeight,
    } = this.state;
    const [eyeX, eyeY, eyeZ] = currentEyePosition;
    viewMatrix
      .setPerspective(30, canvasWidth / canvasHeight, 1, 100)
      .lookAt(eyeX, eyeY, eyeZ, 0, 0, -100, 0, 1, 0);
    gl!.uniformMatrix4fv(uViewMatrix, false, viewMatrix.elements);
    webgl.enablePolygonOffset(gl!, 1.0, 1.0);
    webgl.removeHideSurfaces(gl!);
    webgl.clearColorBuffer(gl!, 0, 0, 0, 1);
    gl!.drawArrays(gl!.TRIANGLES, 0, verticesCount);
  }

  private getNewEyePosition(
    currentEyePosition: number[],
    key: string
  ): number[] {
    const [x, y, z] = currentEyePosition;

    switch (key) {
      case "ArrowLeft":
        return [x - 0.01, y, z];
      case "ArrowRight":
        return [x + 0.01, y, z];
      default:
        return currentEyePosition;
    }
  }
}

export default MultipleTriangles;
