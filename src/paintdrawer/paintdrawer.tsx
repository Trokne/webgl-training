import React, { MouseEvent } from "react";
import { RefObject } from "react";
import * as webgl from "../webgl/webgl";
// @ts-ignore
import fragShader from "./fragment_square.glsl";
// @ts-ignore
import vertexShader from "./vertex_square.glsl";
import { Point } from "../utils/point";

interface IProps {}

interface IState {
  drawAreaReference: RefObject<HTMLCanvasElement>;
  points: Array<Point>;
  colors: Array<Array<number>>;
  gl: WebGLRenderingContext | null;
}

class PaintDrawer extends React.Component<IProps, IState> {
  public constructor(props: IProps) {
    super(props);
    this.state = {
      drawAreaReference: React.createRef<HTMLCanvasElement>(),
      points: [],
      colors: [],
      gl: null,
    };
  }

  public render() {
    return (
      <div>
        <canvas
          id="draw-area"
          width="500px"
          height="500px"
          ref={this.state.drawAreaReference}
          onClick={this.drawPoint.bind(this)}
        >
          Пожалуйста, воспользуйтесь современным браузером с поддержкой canvas!
        </canvas>
      </div>
    );
  }

  public componentDidMount() {
    const gl = webgl.initializeDrawArea(
      this.state.drawAreaReference.current,
      vertexShader,
      fragShader
    );

    webgl.clearColorBuffer(gl, 0, 0, 0, 1);

    this.setState({
      gl: gl,
    });
  }

  private drawPoint(e: MouseEvent<HTMLCanvasElement>) {
    const canvas = e.target as HTMLCanvasElement;
    const points = this.state.points;
    const colors = this.state.colors;
    const gl = this.state.gl as WebGLRenderingContext;
    const x = e.clientX;
    const y = e.clientY;
    const rect = canvas.getBoundingClientRect();

    const normalizedX = (x - rect.left - canvas.width / 2) / (canvas.width / 2);
    const normalizedY =
      (canvas.height / 2 - (y - rect.top)) / (canvas.height / 2);
    points.push(new Point(normalizedX, normalizedY));

    if (normalizedX >= 0.0 && normalizedY >= 0.0) {
      colors.push([1.0, 0.0, 0.0, 1.0]);
    } else if (normalizedX < 0.0 && normalizedY < 0.0) {
      colors.push([0.0, 1.0, 0.0, 1.0]);
    } else {
      colors.push([1.0, 1.0, 1.0, 1.0]);
    }

    const pointPosition = webgl.getAttribute(gl, "a_point_position");
    const pointColor = webgl.getUniform(gl, "u_PointColor");
    webgl.clearColorBuffer(gl, 0, 0, 0, 1);

    points.forEach((point, i) => {
      gl.vertexAttrib3f(pointPosition, point.x, point.y, 0.0);
      gl.uniform4f(
        pointColor,
        colors[i][0],
        colors[i][1],
        colors[i][2],
        colors[i][3]
      );
      gl.drawArrays(gl.POINTS, 0, 1);
    });
  }
}

export default PaintDrawer;
