attribute vec4 a_point_position;

void main() {
    gl_Position = a_point_position;
    gl_PointSize = 10.0;
}