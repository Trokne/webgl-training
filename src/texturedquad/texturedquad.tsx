import React, { RefObject } from "react";
import * as webgl from "../webgl/webgl";
import * as textureLoader from "../webgl/texture/textureloader";
import skyImage from "../assets/sky.jpg";
import circleImage from "../assets/circle.gif";
// @ts-ignore
import fragShader from "./fragment.glsl";
// @ts-ignore
import vertexShader from "./vertex.glsl";
import { TextureParameter } from "../webgl/texture/texture-parameter";

interface IProps {}

interface IState {
  drawAreaReference: RefObject<HTMLCanvasElement>;
  gl: WebGLRenderingContext | null;
  buffer: WebGLBuffer | null;
  verticesCount: number;
  skyTexture: WebGLTexture | null;
  circleTexture: WebGLTexture | null;
}

class TexturedQuad extends React.Component<IProps, IState> {
  public constructor(props: IProps) {
    super(props);
    this.state = {
      verticesCount: 0,
      buffer: null,
      drawAreaReference: React.createRef<HTMLCanvasElement>(),
      gl: null,
      skyTexture: null,
      circleTexture: null,
    };
  }

  public render() {
    return (
      <div>
        <canvas
          id="draw-area"
          width="500px"
          height="500px"
          ref={this.state.drawAreaReference}
        >
          Пожалуйста, воспользуйтесь современным браузером с поддержкой canvas!
        </canvas>
      </div>
    );
  }

  public componentWillUnmount(): void {
    this.state.gl?.deleteTexture(this.state.skyTexture);
    this.state.gl?.deleteTexture(this.state.circleTexture);
    this.state.gl?.deleteBuffer(this.state.buffer);
  }

  public async componentDidMount() {
    const gl = webgl.initializeDrawArea(
      this.state.drawAreaReference.current,
      vertexShader,
      fragShader
    );
    const verticesCount = this.initializeBuffers(gl);
    const skyTexture = await this.loadSkyTexture(gl);
    const circleTexture = await this.loadCircleTexture(gl);

    this.setState(
      {
        gl: gl,
        verticesCount: verticesCount,
        skyTexture: skyTexture,
        circleTexture: circleTexture,
      },
      this.draw.bind(this)
    );
  }

  private draw() {
    const { gl, verticesCount } = this.state;
    webgl.clearColorBuffer(gl!, 0, 0, 0, 1);
    gl?.drawArrays(gl!.TRIANGLE_STRIP, 0, verticesCount);
  }

  private initializeBuffers(gl: WebGLRenderingContext): number {
    const vertices = [
      [-0.5, 0.5, 0.0, 1.0],
      [-0.5, -0.5, 0.0, 0.0],
      [0.5, 0.5, 1.0, 1.0],
      [0.5, -0.5, 1.0, 0.0],
    ];
    const flatVertices = new Float32Array(vertices.flat());
    const buffer = webgl.createAndFillBuffer(
      gl,
      gl.ARRAY_BUFFER,
      new Float32Array(vertices.flat())
    );
    this.setState({
      buffer: buffer,
    });

    this.linkVertexCoordinatesToBuffer(gl, flatVertices);
    this.linkTextureCoodinatesToBuffer(gl, flatVertices);

    return vertices.length;
  }

  private linkVertexCoordinatesToBuffer(
    gl: WebGLRenderingContext,
    vertices: Float32Array
  ) {
    const aPointPosition = webgl.getAttribute(gl, "a_Position");
    const elementSize = vertices.BYTES_PER_ELEMENT;
    webgl.linkAttributeToBuffer(
      gl,
      aPointPosition!,
      2,
      gl.FLOAT,
      false,
      elementSize * 4,
      0
    );
  }

  private linkTextureCoodinatesToBuffer(
    gl: WebGLRenderingContext,
    vertices: Float32Array
  ) {
    const aTexCoord = webgl.getAttribute(gl, "a_TexCoord");
    const elementSize = vertices.BYTES_PER_ELEMENT;
    webgl.linkAttributeToBuffer(
      gl,
      aTexCoord!,
      2,
      gl.FLOAT,
      false,
      elementSize * 4,
      elementSize * 2
    );
  }

  private loadSkyTexture(gl: WebGLRenderingContext) {
    const textureParameters: TextureParameter[] = [
      new TextureParameter(gl.TEXTURE_MIN_FILTER, gl.LINEAR),
    ];
    const uSampler0 = webgl.getUniform(gl, "u_Sampler0");
    return textureLoader.initializeTexture(
      gl,
      uSampler0,
      skyImage,
      0,
      textureParameters
    );
  }

  private loadCircleTexture(gl: WebGLRenderingContext) {
    const textureParameters: TextureParameter[] = [
      new TextureParameter(gl.TEXTURE_MIN_FILTER, gl.LINEAR),
    ];
    const uSampler1 = webgl.getUniform(gl, "u_Sampler1");
    return textureLoader.initializeTexture(
      gl,
      uSampler1,
      circleImage,
      1,
      textureParameters
    );
  }
}

export default TexturedQuad;
