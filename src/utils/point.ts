export class Point {
  constructor(private _x: number, private _y: number) {}

  get x(): number {
    return this._x;
  }

  set x(newCoordinate: number) {
    this._x = newCoordinate;
  }

  get y(): number {
    return this._y;
  }

  set y(newCoordinate: number) {
    this._y = newCoordinate;
  }
}
