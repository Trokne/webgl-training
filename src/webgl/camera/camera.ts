import { Vec3, Vec4 } from "cuon-matrix-ts";
import { Point } from "../../utils/point";
import { MouseCameraParameters } from "./mouse-camera-parameters";
import * as heightmap from "../heightmap";

export class Camera {
  /**
   * Координаты точки наблюдения
   */
  public current: Vec4;
  /**
   * Координаты точки направления взгляда
   */
  public lookAt: Vec4;
  /**
   * Координаты точки, определяющей направление вверх в сцене.
   */
  public up: Vec3;
  public fieldOfView: number;
  public mouseCameraParameters: MouseCameraParameters;

  public constructor() {
    this.current = new Vec4(8, 5, 8, 0.1);
    this.lookAt = new Vec4(0, 0, 0, 0.1);
    this.up = new Vec3(0, 1, 0);
    this.fieldOfView = 40;
    this.mouseCameraParameters = new MouseCameraParameters();
  }

  public setNewUserPositionByKey(keyPressed: string, heightMap: number[][]) {
    const { current, lookAt } = this;

    switch (keyPressed) {
      case "d":
        current.x += current.w;
        lookAt.x += lookAt.w;
        break;
      case "a":
        current.x -= current.w;
        lookAt.x -= lookAt.w;
        break;
      case "w":
        current.z -= current.w;
        lookAt.z -= lookAt.w;
        break;
      case "s":
        current.z += current.w;
        lookAt.z += lookAt.w;
        break;
    }
    const newHeight = heightmap.calculateHeight(
      current.x,
      current.z,
      heightMap
    );
    current.y = newHeight + 2;
  }

  public setNewUserPositionByMouse(point: Point) {
    const { mouseCoordinates, speed } = this.mouseCameraParameters;
    if (mouseCoordinates.x === -10000 && mouseCoordinates.y === -10000) {
      this.mouseCameraParameters.mouseCoordinates = point;
      return;
    }

    const deltaX = mouseCoordinates.x - point.x;
    const deltaY = mouseCoordinates.y - point.y;

    this.lookAt.x += deltaX * speed;
    this.lookAt.y += deltaY * speed;

    this.mouseCameraParameters.mouseCoordinates = point;
  }

  public setMousePressed(isLeftButtonPressed: boolean) {
    this.mouseCameraParameters.isLeftButtonPressed = isLeftButtonPressed;
  }
}
