import { Point } from "../../utils/point";

export class MouseCameraParameters {
  public mouseCoordinates: Point;
  public speed: number;
  public isLeftButtonPressed: boolean;

  public constructor() {
    this.mouseCoordinates = new Point(-10000, -10000);
    this.speed = 0.1;
    this.isLeftButtonPressed = false;
  }
}
