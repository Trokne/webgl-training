export class PressedKey {
  public key: string;
  public isPressed: boolean;

  constructor(key: string) {
    this.key = key;
    this.isPressed = false;
  }
}
