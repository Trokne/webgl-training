export async function read(imageSource: string): Promise<number[][]> {
  return new Promise((resolve) => {
    const image = new Image();
    if (!image || !imageSource) {
      throw new Error(`Изображение ${imageSource} не найдено!`);
    }

    image.onload = () => {
      const heightMap = readHeightMap(image);
      resolve(heightMap);
    };
    image.onerror = (event) => {
      throw new Error("Ошибка загрузки изображения по адресу " + imageSource);
    };
    image.src = imageSource;
  });
}

export function calculateHeight(x: number, z: number, height: number[][]) {
  const i = Math.floor(z);
  const j = Math.floor(x);

  const fi = z - i;
  const fj = x - j;
  const nfi = 1.0 - fi;
  const nfj = 1.0 - fj;

  const resultHeight =
    (heightAt(height, i, j) * nfi + heightAt(height, i + 1, j) * fi) * nfj +
    (heightAt(height, i, j + 1) * nfi + heightAt(height, i + 1, j + 1) * fi) *
      fj;

  return resultHeight;
}

function heightAt(height: number[][], i: number, j: number) {
  if (i >= height.length || j >= height[i].length) {
    return 0;
  }

  return height[i][j];
}

function readHeightMap(image: HTMLImageElement): number[][] {
  const heightMap = [];
  const { height, width } = image;
  if (height !== width || !isExponentTwo(height) || !isExponentTwo(width)) {
    throw new Error(
      "Ширина и высота карты высот должны быть равны и являться степенью двойки"
    );
  }

  const imageContext = drawPixels(image);
  for (let i = 0; i < height; i++) {
    const pixelRow = [];
    for (let j = 0; j < width; j++) {
      const pixel = imageContext.getImageData(i, j, 1, 1).data;
      const averagedPixel = (pixel[0] + pixel[1] + pixel[2]) / (210 * 3);
      pixelRow.push(averagedPixel);
    }
    heightMap.push(pixelRow);
  }

  return heightMap;
}

function drawPixels(image: HTMLImageElement) {
  const canvas = document.createElement("canvas");
  const context = canvas.getContext("2d");

  if (!context) {
    throw new Error("Ошибка создания canvas для Image");
  }
  context.drawImage(image, 0, 0);
  return context;
}

function isExponentTwo(num: number) {
  return !(num & (num - 1));
}
