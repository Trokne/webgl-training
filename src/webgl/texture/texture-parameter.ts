export class TextureParameter {
  public name: GLenum;
  public value: GLint;

  constructor(name: GLenum, value: GLint) {
    this.name = name;
    this.value = value;
  }
}
