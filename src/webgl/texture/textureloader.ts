import { TextureParameter } from "./texture-parameter";

function getImageExtension(imageSource: string) {
  return imageSource.split(".").pop();
}

export function initializeTexture(
  gl: WebGLRenderingContext,
  uSampler: WebGLUniformLocation,
  imageSource: string,
  textureSlotNumber: number,
  textureParameters: TextureParameter[]
): WebGLTexture {
  return new Promise((resolve) => {
    const texture = gl.createTexture();
    if (!texture) {
      throw new Error("Ошибка создания объекта для будущей текстуры!");
    }

    const image = new Image();
    if (!image || !imageSource) {
      throw new Error("Ошибка создания объекта для будущего изображения!");
    }

    const imageExtension = getImageExtension(imageSource);

    image.onload = () => {
      loadTexture(
        gl,
        texture,
        uSampler,
        image,
        imageExtension,
        textureSlotNumber,
        textureParameters
      );
      resolve(texture);
    };
    image.onerror = (event) => {
      throw new Error("Ошибка загрузки изображения по адресу " + imageSource);
    };
    image.src = imageSource;
  });
}

function loadTexture(
  gl: WebGLRenderingContext,
  texture: WebGLTexture,
  uSampler: WebGLUniformLocation,
  image: TexImageSource,
  imageExtension: string | undefined,
  textureSlotNumber: number,
  textureParameters: TextureParameter[]
) {
  // Flip the image's y axis
  gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
  setTextureToSlot(gl, textureSlotNumber);
  gl.bindTexture(gl.TEXTURE_2D, texture);
  setTextureParameters(gl, textureParameters);
  setImageToTexture(gl, image, imageExtension);
  // Set the texture unit 0 to the sampler
  gl.uniform1i(uSampler, textureSlotNumber);
}

function setTextureParameters(
  gl: WebGLRenderingContext,
  textureParameters: TextureParameter[]
) {
  textureParameters.forEach((param) =>
    gl.texParameteri(gl.TEXTURE_2D, param.name, param.value)
  );
}

function setImageToTexture(
  gl: WebGLRenderingContext,
  image: TexImageSource,
  imageExtension: string | undefined
) {
  let imageFormat: GLenum;
  const lowerCaseExtension = imageExtension?.toLowerCase();
  if (lowerCaseExtension === "png" || lowerCaseExtension === "gif") {
    imageFormat = gl.RGBA;
  } else {
    imageFormat = gl.RGB;
  }

  gl.texImage2D(
    gl.TEXTURE_2D,
    0,
    imageFormat,
    imageFormat,
    gl.UNSIGNED_BYTE,
    image
  );
}

function setTextureToSlot(
  gl: WebGLRenderingContext,
  textureSlotNumber: number
) {
  switch (textureSlotNumber) {
    case 0:
      gl.activeTexture(gl.TEXTURE0);
      break;
    case 1:
      gl.activeTexture(gl.TEXTURE1);
      break;
    case 2:
      gl.activeTexture(gl.TEXTURE2);
      break;
    case 3:
      gl.activeTexture(gl.TEXTURE3);
      break;
    case 4:
      gl.activeTexture(gl.TEXTURE4);
      break;
    case 5:
      gl.activeTexture(gl.TEXTURE5);
      break;
    case 6:
      gl.activeTexture(gl.TEXTURE6);
      break;
    case 7:
      gl.activeTexture(gl.TEXTURE7);
      break;
    default:
      throw new Error("Необходимо дополнить switch с текстурными слотами!");
  }
}
