import * as webgl from "./webgl";
import { Mat4 } from "cuon-matrix-ts";

export function setMatrix4ToUniform(
  gl: WebGLRenderingContext,
  name: string,
  values: Mat4
) {
  const link = webgl.getUniform(gl, name);
  gl.uniformMatrix4fv(link, false, values.elements);
}
