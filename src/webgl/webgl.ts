import { getWebGLContext, initShaders } from "./cuon-utils";

export function initializeArrayBuffer(
  gl: WebGLRenderingContext,
  paramName: string,
  data: BufferSource,
  size: number,
  dataType: GLenum
) {
  const buffer = createAndFillBuffer(gl, gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);
  const attribute = getAttribute(gl, paramName);
  linkAttributeToBuffer(gl, attribute, size, dataType, false, 0, 0);

  return buffer;
}

export function unbindBuffer(gl: WebGLRenderingContext) {
  gl.bindBuffer(gl.ARRAY_BUFFER, null);
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
}

export function createBuffer(
  gl: WebGLRenderingContext,
  bufferType: GLenum
): WebGLBuffer {
  const buffer = gl.createBuffer();
  if (!buffer) {
    throw new Error("Ошибка создания буфферного объекта");
  }
  gl.bindBuffer(bufferType, buffer);

  return buffer;
}

export function createAndFillBuffer(
  gl: WebGLRenderingContext,
  bufferType: GLenum,
  value: BufferSource,
  drawType: GLenum = gl.STATIC_DRAW
): WebGLBuffer {
  const buffer = createBuffer(gl, bufferType);
  gl.bufferData(bufferType, value, drawType);

  return buffer;
}

export function linkAttributeToBuffer(
  gl: WebGLRenderingContext,
  link: GLint,
  size: number,
  type: GLenum = gl.FLOAT,
  normalized: boolean = false,
  strideInBytes: number = 0,
  offsetInBytes: number = 0
) {
  gl.vertexAttribPointer(
    link,
    size,
    type,
    normalized,
    strideInBytes,
    offsetInBytes
  );
  gl.enableVertexAttribArray(link);
}

export function getAttribute(gl: WebGLRenderingContext, name: string): GLint {
  return getAttributeByProgram(gl, gl.getParameter(gl.CURRENT_PROGRAM), name);
}

export function getAttributeByProgram(
  gl: WebGLRenderingContext,
  program: WebGLProgram,
  name: string
): GLint {
  const location = gl.getAttribLocation(program, name);
  if (location < 0) {
    throw new Error(`Ошибка инициализации attribute переменной ${name}`);
  }
  return location;
}

export function getUniform(
  gl: WebGLRenderingContext,
  name: string
): WebGLUniformLocation {
  return getUniformByProgram(gl, gl.getParameter(gl.CURRENT_PROGRAM), name);
}

export function getUniformByProgram(
  gl: WebGLRenderingContext,
  program: WebGLProgram,
  name: string
): WebGLUniformLocation {
  const location = gl.getUniformLocation(program, name);
  if (!location) {
    throw new Error(`Ошибка инициализации uniform переменной ${name}`);
  }
  return location;
}

export function initializeDrawArea(
  drawArea: HTMLCanvasElement | null,
  vertexShader: any,
  fragShader: any
): WebGLRenderingContext {
  const gl = getWebGLContext(drawArea, true);
  initShaders(gl, vertexShader, fragShader);
  return gl;
}

export function clearColorBuffer(
  gl: WebGLRenderingContext,
  red: number,
  green: number,
  blue: number,
  alpha: number
) {
  gl.clearColor(red, green, blue, alpha);
  gl.clear(gl.COLOR_BUFFER_BIT);
}

export function removeHideSurfaces(gl: WebGLRenderingContext) {
  gl.enable(gl.DEPTH_TEST);
  gl.clear(gl.DEPTH_BUFFER_BIT);
}

/**
 * Определяет поправку к координате Z для каждой вершины, которая будет нарисована после вызова этого метода
 * Поправка вычисляется по формуле m * factor + r * units,
 * где m - наклон треугольника относительно линии взгляда,
 * r – наименьшая разница между двумя значениями координаты Z, которые могут оказаться неразличимыми для WebGL.
 * @param gl
 * @param factor
 * @param units
 */
export function enablePolygonOffset(
  gl: WebGLRenderingContext,
  factor: number,
  units: number
) {
  gl.enable(gl.POLYGON_OFFSET_FILL);
  gl.polygonOffset(factor, units);
}

export function createEmptyArrayBuffer(
  gl: WebGLRenderingContext,
  attributeLink: GLint,
  num: number,
  type: GLenum
) {
  const buffer = createBuffer(gl, gl.ARRAY_BUFFER);
  linkAttributeToBuffer(gl, attributeLink, num, type);
  return buffer;
}
